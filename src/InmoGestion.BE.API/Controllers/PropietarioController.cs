﻿using InmoGestion.BE.Models.DTOs;
using InmoGestion.BE.Services.Interface;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace InmoGestion.BE.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PropietarioController : ControllerBase
    {
        #region Dependencias
        private readonly IPropietarioService _propietarioService;
        #endregion

        #region Constructor
        public PropietarioController(
           IPropietarioService propietarioService
           )
        {
            _propietarioService = propietarioService;
        }
        #endregion

        #region Endpoints
        /// <summary>
        /// Propietario
        /// </summary>
        /// <remarks>
        /// Obtiene un Propietario por Id 
        /// </remarks>
        /// <param name="idEmpresa"></param>
        /// <returns></returns>
        /// <response code="200">Devuelve la entidad</response>
        /// <response code="204">Vacio</response>
        /// <response code="500">En caso de alguno error interno</response>
        [SwaggerOperation(Summary = "Obtiene un Propietario por Id ")]
        [HttpGet("GetById")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(PropietarioDTO))]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetById(int id)
        {
            var dto = new PropietarioDTO();
            //Al no tener conexion en el appSetings va a dar error
            try
            {
                dto = _propietarioService.GetById(id);
            }
            catch (Exception)
            {
                return NoContent();
            }

            return Ok(dto);
        }

        /// <summary>
        /// Propietario
        /// </summary>
        /// <remarks>
        /// Obtiene un Propietario por Id 
        /// </remarks>
        /// <param name="idEmpresa"></param>
        /// <returns></returns>
        /// <response code="200">Devuelve la entidad</response>
        /// <response code="204">Vacio</response>
        /// <response code="500">En caso de alguno error interno</response>
        [SwaggerOperation(Summary = "Obtiene un lista Propietario por Id ")]
        [HttpGet("GetAllBy")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<PropietarioDTO>))]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetAllBy(int id)
        {
            var dto = new List<PropietarioDTO>();
            //Al no tener conexion en el appSetings va a dar error
            try
            {
                dto = _propietarioService.GetAllBy(id);
            }
            catch (Exception)
            {
                return NoContent();
            }

            return Ok(dto);
        }

        /// <summary>
        /// Propietario
        /// </summary>
        /// <remarks>
        /// Obtiene un Propietario por Id 
        /// </remarks>
        /// <param name="idEmpresa"></param>
        /// <returns></returns>
        /// <response code="200">Devuelve la entidad</response>
        /// <response code="204">Vacio</response>
        /// <response code="500">En caso de alguno error interno</response>
        [SwaggerOperation(Summary = "Obtiene un lista Propietario ")]
        [HttpGet("GetAll")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<PropietarioDTO>))]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetAll()
        {
            var dto = new List<PropietarioDTO>();
            //Al no tener conexion en el appSetings va a dar error
            try
            {
                dto = _propietarioService.GetAll();
            }
            catch (Exception)
            {
                return NoContent();
            }

            return Ok(dto);
        }






        /// <summary>
        /// Propietario
        /// </summary>
        /// <remarks>
        /// Guarda un PropietarioDTO 
        /// </remarks>
        /// <param name="model"></param>
        /// <returns></returns>
        /// <response code="200">Guarda un PropietarioDTO</response>
        /// <response code="204">Vacio</response>
        /// <response code="500">En caso de alguno error interno</response>
        [SwaggerOperation(Summary = "Guarda un PropietarioDTO")]
        [HttpPost("Save")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(bool))]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> Save(PropietarioDTO model)
        {
            bool result;
            //Al no tener conexion en el appSetings va a dar error
            try
            {
                result = _propietarioService.Save(model);
            }
            catch (Exception)
            {
                return NoContent();
            }

            return Ok(result);
        }

        /// <summary>
        /// Propietario
        /// </summary>
        /// <remarks>
        /// Elimina un PropietarioDTO 
        /// </remarks>
        /// <param name="model"></param>
        /// <returns></returns>
        /// <response code="200">Elimina un PropietarioDTO</response>
        /// <response code="204">Vacio</response>
        /// <response code="500">En caso de alguno error interno</response>
        [SwaggerOperation(Summary = "Elimina un PropietarioDTO")]
        [HttpDelete("Delete")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(bool))]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> Delete(int id)
        {
            bool result;
            //Al no tener conexion en el appSetings va a dar error
            try
            {
                result = _propietarioService.Delete(id);
            }
            catch (Exception)
            {
                return NoContent();
            }

            return Ok(result);
        }


        /// <summary>
        /// Propietario
        /// </summary>
        /// <remarks>
        /// Actualiza un PropietarioDTO 
        /// </remarks>
        /// <param name="model"></param>
        /// <returns></returns>
        /// <response code="200">Actualiza un PropietarioDTO</response>
        /// <response code="204">Vacio</response>
        /// <response code="500">En caso de alguno error interno</response>
        [SwaggerOperation(Summary = "Actualiza un PropietarioDTO")]
        [HttpPut("Update")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(bool))]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> Update(int id)
        {
            bool result;
            //Al no tener conexion en el appSetings va a dar error
            try
            {
                result = _propietarioService.Update(id);
            }
            catch (Exception)
            {
                return NoContent();
            }

            return Ok(result);
        }


        #endregion
    }
}
