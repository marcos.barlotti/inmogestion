﻿using InmoGestion.BE.Models.DTOs;
using InmoGestion.BE.Services.Interface;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace InmoGestion.BE.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TipoPropiedadController : ControllerBase
    {
        #region Dependencias
        private readonly ITipoPropiedadService _tipoPropiedadService;
        #endregion

        #region Constructor
        public TipoPropiedadController(
           ITipoPropiedadService tipoPropiedadService
           )
        {
            _tipoPropiedadService = tipoPropiedadService;
        }
        #endregion

        #region Endpoints
        /// <summary>
        /// TipoPropiedad
        /// </summary>
        /// <remarks>
        /// Obtiene un TipoPropiedad por Id 
        /// </remarks>
        /// <param name="idEmpresa"></param>
        /// <returns></returns>
        /// <response code="200">Devuelve la entidad</response>
        /// <response code="204">Vacio</response>
        /// <response code="500">En caso de alguno error interno</response>
        [SwaggerOperation(Summary = "Obtiene un TipoPropiedad por Id ")]
        [HttpGet("GetById")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(TipoPropiedadDTO))]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetById(int id)
        {
            var dto = new TipoPropiedadDTO();
            //Al no tener conexion en el appSetings va a dar error
            try
            {
                dto = _tipoPropiedadService.GetById(id);
            }
            catch (Exception)
            {
                return NoContent();
            }

            return Ok(dto);
        }

        /// <summary>
        /// TipoPropiedad
        /// </summary>
        /// <remarks>
        /// Obtiene un TipoPropiedad por Id 
        /// </remarks>
        /// <param name="idEmpresa"></param>
        /// <returns></returns>
        /// <response code="200">Devuelve la entidad</response>
        /// <response code="204">Vacio</response>
        /// <response code="500">En caso de alguno error interno</response>
        [SwaggerOperation(Summary = "Obtiene un lista TipoPropiedad por Id ")]
        [HttpGet("GetAllBy")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<TipoPropiedadDTO>))]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetAllBy(int id)
        {
            var dto = new List<TipoPropiedadDTO>();
            //Al no tener conexion en el appSetings va a dar error
            try
            {
                dto = _tipoPropiedadService.GetAllBy(id);
            }
            catch (Exception)
            {
                return NoContent();
            }

            return Ok(dto);
        }

        /// <summary>
        /// TipoPropiedad
        /// </summary>
        /// <remarks>
        /// Obtiene un TipoPropiedad por Id 
        /// </remarks>
        /// <param name="idEmpresa"></param>
        /// <returns></returns>
        /// <response code="200">Devuelve la entidad</response>
        /// <response code="204">Vacio</response>
        /// <response code="500">En caso de alguno error interno</response>
        [SwaggerOperation(Summary = "Obtiene un lista TipoPropiedad ")]
        [HttpGet("GetAll")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<TipoPropiedadDTO>))]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetAll()
        {
            var dto = new List<TipoPropiedadDTO>();
            //Al no tener conexion en el appSetings va a dar error
            try
            {
                dto = _tipoPropiedadService.GetAll();
            }
            catch (Exception)
            {
                return NoContent();
            }

            return Ok(dto);
        }






        /// <summary>
        /// TipoPropiedad
        /// </summary>
        /// <remarks>
        /// Guarda un TipoPropiedadDTO 
        /// </remarks>
        /// <param name="model"></param>
        /// <returns></returns>
        /// <response code="200">Guarda un TipoPropiedadDTO</response>
        /// <response code="204">Vacio</response>
        /// <response code="500">En caso de alguno error interno</response>
        [SwaggerOperation(Summary = "Guarda un TipoPropiedadDTO")]
        [HttpPost("Save")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(bool))]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> Save(TipoPropiedadDTO model)
        {
            bool result;
            //Al no tener conexion en el appSetings va a dar error
            try
            {
                result = _tipoPropiedadService.Save(model);
            }
            catch (Exception)
            {
                return NoContent();
            }

            return Ok(result);
        }

        /// <summary>
        /// TipoPropiedad
        /// </summary>
        /// <remarks>
        /// Elimina un TipoPropiedadDTO 
        /// </remarks>
        /// <param name="model"></param>
        /// <returns></returns>
        /// <response code="200">Elimina un TipoPropiedadDTO</response>
        /// <response code="204">Vacio</response>
        /// <response code="500">En caso de alguno error interno</response>
        [SwaggerOperation(Summary = "Elimina un TipoPropiedadDTO")]
        [HttpDelete("Delete")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(bool))]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> Delete(int id)
        {
            bool result;
            //Al no tener conexion en el appSetings va a dar error
            try
            {
                result = _tipoPropiedadService.Delete(id);
            }
            catch (Exception)
            {
                return NoContent();
            }

            return Ok(result);
        }


        /// <summary>
        /// TipoPropiedad
        /// </summary>
        /// <remarks>
        /// Actualiza un TipoPropiedadDTO 
        /// </remarks>
        /// <param name="model"></param>
        /// <returns></returns>
        /// <response code="200">Actualiza un TipoPropiedadDTO</response>
        /// <response code="204">Vacio</response>
        /// <response code="500">En caso de alguno error interno</response>
        [SwaggerOperation(Summary = "Actualiza un TipoPropiedadDTO")]
        [HttpPut("Update")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(bool))]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> Update(int id)
        {
            bool result;
            //Al no tener conexion en el appSetings va a dar error
            try
            {
                result = _tipoPropiedadService.Update(id);
            }
            catch (Exception)
            {
                return NoContent();
            }

            return Ok(result);
        }


        #endregion
    }
}
