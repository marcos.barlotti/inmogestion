﻿using InmoGestion.BE.Models.DTOs;
using InmoGestion.BE.Services.Interface;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace InmoGestion.BE.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PersonaController : ControllerBase
    {
        #region Dependencias
        private readonly IPersonaService _personaService;
        #endregion

        #region Constructor
        public PersonaController(
           IPersonaService personaService
           )
        {
            _personaService = personaService;
        }
        #endregion

        #region Endpoints
        /// <summary>
        /// Persona
        /// </summary>
        /// <remarks>
        /// Obtiene un Persona por Id 
        /// </remarks>
        /// <param name="idEmpresa"></param>
        /// <returns></returns>
        /// <response code="200">Devuelve la entidad</response>
        /// <response code="204">Vacio</response>
        /// <response code="500">En caso de alguno error interno</response>
        [SwaggerOperation(Summary = "Obtiene un Persona por Id ")]
        [HttpGet("GetById")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(PersonaDTO))]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetById(int id)
        {
            var dto = new PersonaDTO();
            //Al no tener conexion en el appSetings va a dar error
            try
            {
                dto = _personaService.GetById(id);
            }
            catch (Exception)
            {
                return NoContent();
            }

            return Ok(dto);
        }

        /// <summary>
        /// Persona
        /// </summary>
        /// <remarks>
        /// Obtiene un Persona por Id 
        /// </remarks>
        /// <param name="idEmpresa"></param>
        /// <returns></returns>
        /// <response code="200">Devuelve la entidad</response>
        /// <response code="204">Vacio</response>
        /// <response code="500">En caso de alguno error interno</response>
        [SwaggerOperation(Summary = "Obtiene un lista Persona por Id ")]
        [HttpGet("GetAllBy")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<PersonaDTO>))]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetAllBy(int id)
        {
            var dto = new List<PersonaDTO>();
            //Al no tener conexion en el appSetings va a dar error
            try
            {
                dto = _personaService.GetAllBy(id);
            }
            catch (Exception)
            {
                return NoContent();
            }

            return Ok(dto);
        }

        /// <summary>
        /// Persona
        /// </summary>
        /// <remarks>
        /// Obtiene un Persona por Id 
        /// </remarks>
        /// <param name="idEmpresa"></param>
        /// <returns></returns>
        /// <response code="200">Devuelve la entidad</response>
        /// <response code="204">Vacio</response>
        /// <response code="500">En caso de alguno error interno</response>
        [SwaggerOperation(Summary = "Obtiene un lista Persona ")]
        [HttpGet("GetAll")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<PersonaDTO>))]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetAll()
        {
            var dto = new List<PersonaDTO>();
            //Al no tener conexion en el appSetings va a dar error
            try
            {
                dto = _personaService.GetAll();
            }
            catch (Exception)
            {
                return NoContent();
            }

            return Ok(dto);
        }






        /// <summary>
        /// Persona
        /// </summary>
        /// <remarks>
        /// Guarda un PersonaDTO 
        /// </remarks>
        /// <param name="model"></param>
        /// <returns></returns>
        /// <response code="200">Guarda un PersonaDTO</response>
        /// <response code="204">Vacio</response>
        /// <response code="500">En caso de alguno error interno</response>
        [SwaggerOperation(Summary = "Guarda un PersonaDTO")]
        [HttpPost("Save")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(bool))]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> Save(PersonaDTO model)
        {
            bool result;
            //Al no tener conexion en el appSetings va a dar error
            try
            {
                result = _personaService.Save(model);
            }
            catch (Exception)
            {
                return NoContent();
            }

            return Ok(result);
        }

        /// <summary>
        /// Persona
        /// </summary>
        /// <remarks>
        /// Elimina un PersonaDTO 
        /// </remarks>
        /// <param name="model"></param>
        /// <returns></returns>
        /// <response code="200">Elimina un PersonaDTO</response>
        /// <response code="204">Vacio</response>
        /// <response code="500">En caso de alguno error interno</response>
        [SwaggerOperation(Summary = "Elimina un PersonaDTO")]
        [HttpDelete("Delete")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(bool))]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> Delete(int id)
        {
            bool result;
            //Al no tener conexion en el appSetings va a dar error
            try
            {
                result = _personaService.Delete(id);
            }
            catch (Exception)
            {
                return NoContent();
            }

            return Ok(result);
        }


        /// <summary>
        /// Persona
        /// </summary>
        /// <remarks>
        /// Actualiza un PersonaDTO 
        /// </remarks>
        /// <param name="model"></param>
        /// <returns></returns>
        /// <response code="200">Actualiza un PersonaDTO</response>
        /// <response code="204">Vacio</response>
        /// <response code="500">En caso de alguno error interno</response>
        [SwaggerOperation(Summary = "Actualiza un PersonaDTO")]
        [HttpPut("Update")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(bool))]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> Update(int id)
        {
            bool result;
            //Al no tener conexion en el appSetings va a dar error
            try
            {
                result = _personaService.Update(id);
            }
            catch (Exception)
            {
                return NoContent();
            }

            return Ok(result);
        }


        #endregion
    }
}
