﻿using InmoGestion.BE.Models.DTOs;
using InmoGestion.BE.Services.Interface;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace InmoGestion.BE.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TipoGarantiaController : ControllerBase
    {
        #region Dependencias
        private readonly ITipoGarantiaService _tipoGarantiaService;
        #endregion

        #region Constructor
        public TipoGarantiaController(
           ITipoGarantiaService tipoGarantiaService
           )
        {
            _tipoGarantiaService = tipoGarantiaService;
        }
        #endregion

        #region Endpoints
        /// <summary>
        /// TipoGarantia
        /// </summary>
        /// <remarks>
        /// Obtiene un TipoGarantia por Id 
        /// </remarks>
        /// <param name="idEmpresa"></param>
        /// <returns></returns>
        /// <response code="200">Devuelve la entidad</response>
        /// <response code="204">Vacio</response>
        /// <response code="500">En caso de alguno error interno</response>
        [SwaggerOperation(Summary = "Obtiene un TipoGarantia por Id ")]
        [HttpGet("GetById")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(TipoGarantiaDTO))]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetById(int id)
        {
            var dto = new TipoGarantiaDTO();
            //Al no tener conexion en el appSetings va a dar error
            try
            {
                dto = _tipoGarantiaService.GetById(id);
            }
            catch (Exception)
            {
                return NoContent();
            }

            return Ok(dto);
        }

        /// <summary>
        /// TipoGarantia
        /// </summary>
        /// <remarks>
        /// Obtiene un TipoGarantia por Id 
        /// </remarks>
        /// <param name="idEmpresa"></param>
        /// <returns></returns>
        /// <response code="200">Devuelve la entidad</response>
        /// <response code="204">Vacio</response>
        /// <response code="500">En caso de alguno error interno</response>
        [SwaggerOperation(Summary = "Obtiene un lista TipoGarantia por Id ")]
        [HttpGet("GetAllBy")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<TipoGarantiaDTO>))]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetAllBy(int id)
        {
            var dto = new List<TipoGarantiaDTO>();
            //Al no tener conexion en el appSetings va a dar error
            try
            {
                dto = _tipoGarantiaService.GetAllBy(id);
            }
            catch (Exception)
            {
                return NoContent();
            }

            return Ok(dto);
        }

        /// <summary>
        /// TipoGarantia
        /// </summary>
        /// <remarks>
        /// Obtiene un TipoGarantia por Id 
        /// </remarks>
        /// <param name="idEmpresa"></param>
        /// <returns></returns>
        /// <response code="200">Devuelve la entidad</response>
        /// <response code="204">Vacio</response>
        /// <response code="500">En caso de alguno error interno</response>
        [SwaggerOperation(Summary = "Obtiene un lista TipoGarantia ")]
        [HttpGet("GetAll")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<TipoGarantiaDTO>))]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetAll()
        {
            var dto = new List<TipoGarantiaDTO>();
            //Al no tener conexion en el appSetings va a dar error
            try
            {
                dto = _tipoGarantiaService.GetAll();
            }
            catch (Exception)
            {
                return NoContent();
            }

            return Ok(dto);
        }






        /// <summary>
        /// TipoGarantia
        /// </summary>
        /// <remarks>
        /// Guarda un TipoGarantiaDTO 
        /// </remarks>
        /// <param name="model"></param>
        /// <returns></returns>
        /// <response code="200">Guarda un TipoGarantiaDTO</response>
        /// <response code="204">Vacio</response>
        /// <response code="500">En caso de alguno error interno</response>
        [SwaggerOperation(Summary = "Guarda un TipoGarantiaDTO")]
        [HttpPost("Save")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(bool))]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> Save(TipoGarantiaDTO model)
        {
            bool result;
            //Al no tener conexion en el appSetings va a dar error
            try
            {
                result = _tipoGarantiaService.Save(model);
            }
            catch (Exception)
            {
                return NoContent();
            }

            return Ok(result);
        }

        /// <summary>
        /// TipoGarantia
        /// </summary>
        /// <remarks>
        /// Elimina un TipoGarantiaDTO 
        /// </remarks>
        /// <param name="model"></param>
        /// <returns></returns>
        /// <response code="200">Elimina un TipoGarantiaDTO</response>
        /// <response code="204">Vacio</response>
        /// <response code="500">En caso de alguno error interno</response>
        [SwaggerOperation(Summary = "Elimina un TipoGarantiaDTO")]
        [HttpDelete("Delete")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(bool))]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> Delete(int id)
        {
            bool result;
            //Al no tener conexion en el appSetings va a dar error
            try
            {
                result = _tipoGarantiaService.Delete(id);
            }
            catch (Exception)
            {
                return NoContent();
            }

            return Ok(result);
        }


        /// <summary>
        /// TipoGarantia
        /// </summary>
        /// <remarks>
        /// Actualiza un TipoGarantiaDTO 
        /// </remarks>
        /// <param name="model"></param>
        /// <returns></returns>
        /// <response code="200">Actualiza un TipoGarantiaDTO</response>
        /// <response code="204">Vacio</response>
        /// <response code="500">En caso de alguno error interno</response>
        [SwaggerOperation(Summary = "Actualiza un TipoGarantiaDTO")]
        [HttpPut("Update")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(bool))]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> Update(int id)
        {
            bool result;
            //Al no tener conexion en el appSetings va a dar error
            try
            {
                result = _tipoGarantiaService.Update(id);
            }
            catch (Exception)
            {
                return NoContent();
            }

            return Ok(result);
        }


        #endregion
    }
}
