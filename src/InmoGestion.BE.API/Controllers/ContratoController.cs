﻿using InmoGestion.BE.Models.DTOs;
using InmoGestion.BE.Services.Interface;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace InmoGestion.BE.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ContratoController : ControllerBase
    {
        #region Dependencias
        private readonly IContratoService _contratoService;
        #endregion

        #region Constructor
        public ContratoController(
           IContratoService contratoService
           )
        {
            _contratoService = contratoService;
        }
        #endregion

        #region Endpoints
        /// <summary>
        /// Contrato
        /// </summary>
        /// <remarks>
        /// Obtiene un Contrato por Id 
        /// </remarks>
        /// <param name="idEmpresa"></param>
        /// <returns></returns>
        /// <response code="200">Devuelve la entidad</response>
        /// <response code="204">Vacio</response>
        /// <response code="500">En caso de alguno error interno</response>
        [SwaggerOperation(Summary = "Obtiene un Contrato por Id ")]
        [HttpGet("GetById")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ContratoDTO))]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetById(int id)
        {
            var dto = new ContratoDTO();
            //Al no tener conexion en el appSetings va a dar error
            try
            {
                dto = _contratoService.GetById(id);
            }
            catch (Exception)
            {
                return NoContent();
            }

            return Ok(dto);
        }

        /// <summary>
        /// Contrato
        /// </summary>
        /// <remarks>
        /// Obtiene un Contrato por Id 
        /// </remarks>
        /// <param name="idEmpresa"></param>
        /// <returns></returns>
        /// <response code="200">Devuelve la entidad</response>
        /// <response code="204">Vacio</response>
        /// <response code="500">En caso de alguno error interno</response>
        [SwaggerOperation(Summary = "Obtiene un lista Contrato por Id ")]
        [HttpGet("GetAllBy")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<ContratoDTO>))]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetAllBy(int id)
        {
            var dto = new List<ContratoDTO>();
            //Al no tener conexion en el appSetings va a dar error
            try
            {
                dto = _contratoService.GetAllBy(id);
            }
            catch (Exception)
            {
                return NoContent();
            }

            return Ok(dto);
        }

        /// <summary>
        /// Contrato
        /// </summary>
        /// <remarks>
        /// Obtiene un Contrato por Id 
        /// </remarks>
        /// <param name="idEmpresa"></param>
        /// <returns></returns>
        /// <response code="200">Devuelve la entidad</response>
        /// <response code="204">Vacio</response>
        /// <response code="500">En caso de alguno error interno</response>
        [SwaggerOperation(Summary = "Obtiene un lista Contrato ")]
        [HttpGet("GetAll")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<ContratoDTO>))]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetAll()
        {
            var dto = new List<ContratoDTO>();
            //Al no tener conexion en el appSetings va a dar error
            try
            {
                dto = _contratoService.GetAll();
            }
            catch (Exception)
            {
                return NoContent();
            }

            return Ok(dto);
        }






        /// <summary>
        /// Contrato
        /// </summary>
        /// <remarks>
        /// Guarda un ContratoDTO 
        /// </remarks>
        /// <param name="model"></param>
        /// <returns></returns>
        /// <response code="200">Guarda un ContratoDTO</response>
        /// <response code="204">Vacio</response>
        /// <response code="500">En caso de alguno error interno</response>
        [SwaggerOperation(Summary = "Guarda un ContratoDTO")]
        [HttpPost("Save")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(bool))]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> Save(ContratoDTO model)
        {
            bool result;
            //Al no tener conexion en el appSetings va a dar error
            try
            {
                result = _contratoService.Save(model);
            }
            catch (Exception)
            {
                return NoContent();
            }

            return Ok(result);
        }

        /// <summary>
        /// Contrato
        /// </summary>
        /// <remarks>
        /// Elimina un ContratoDTO 
        /// </remarks>
        /// <param name="model"></param>
        /// <returns></returns>
        /// <response code="200">Elimina un ContratoDTO</response>
        /// <response code="204">Vacio</response>
        /// <response code="500">En caso de alguno error interno</response>
        [SwaggerOperation(Summary = "Elimina un ContratoDTO")]
        [HttpDelete("Delete")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(bool))]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> Delete(int id)
        {
            bool result;
            //Al no tener conexion en el appSetings va a dar error
            try
            {
                result = _contratoService.Delete(id);
            }
            catch (Exception)
            {
                return NoContent();
            }

            return Ok(result);
        }


        /// <summary>
        /// Contrato
        /// </summary>
        /// <remarks>
        /// Actualiza un ContratoDTO 
        /// </remarks>
        /// <param name="model"></param>
        /// <returns></returns>
        /// <response code="200">Actualiza un ContratoDTO</response>
        /// <response code="204">Vacio</response>
        /// <response code="500">En caso de alguno error interno</response>
        [SwaggerOperation(Summary = "Actualiza un ContratoDTO")]
        [HttpPut("Update")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(bool))]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> Update(int id)
        {
            bool result;
            //Al no tener conexion en el appSetings va a dar error
            try
            {
                result = _contratoService.Update(id);
            }
            catch (Exception)
            {
                return NoContent();
            }

            return Ok(result);
        }


        #endregion
    }
}
