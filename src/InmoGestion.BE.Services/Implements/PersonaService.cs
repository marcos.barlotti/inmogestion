﻿using AutoMapper;
using InmoGestion.BE.Models.DTOs;
using InmoGestion.BE.Models.Entities;
using InmoGestion.BE.Services.Interface;
using InmoGestion.BE.Repositories.Interface;

namespace InmoGestion.BE.Services.Implements
{
    public class PersonaService : IPersonaService
    {
        #region Dependencias
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;
        #endregion

        #region Constructor
        public PersonaService(
            IMapper mapper,
            IUnitOfWork unitOfWork
            )
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }
        #endregion
        public bool Delete(int id)
        {
            try
            {
                _unitOfWork.Persona.Delete(id);
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                return false;
            }

            return true;
        }

        public PersonaDTO GetById(int id)
        {
            var model = _unitOfWork.Persona.GetById(id);
            return _mapper.Map<PersonaDTO>(model);
        }

        public List<PersonaDTO> GetAll()
        {
            var model = _unitOfWork.Persona.GetAll();
            return _mapper.Map<List<PersonaDTO>>(model);
        }

        public List<PersonaDTO> GetAllBy(int id)
        {
            var model = _unitOfWork.Persona.GetAllBy(x => x.Id == id);
            return _mapper.Map<List<PersonaDTO>>(model);
        }



        public bool Save(PersonaDTO model)
        {
            try
            {
                var entitie = _mapper.Map<Persona>(model);
                _unitOfWork.Persona.Save(entitie);
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                return false;
            }

            return true;
        }

        public bool Update(int id)
        {
            try
            {
                _unitOfWork.Persona.Update(id);
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                return false;
            }

            return true;
        }
    }
}
