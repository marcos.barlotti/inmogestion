﻿using AutoMapper;
using InmoGestion.BE.Models.DTOs;
using InmoGestion.BE.Models.Entities;
using InmoGestion.BE.Services.Interface;
using InmoGestion.BE.Repositories.Interface;

namespace InmoGestion.BE.Services.Implements
{
    public class PropietarioService : IPropietarioService
    {
        #region Dependencias
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;
        #endregion

        #region Constructor
        public PropietarioService(
            IMapper mapper,
            IUnitOfWork unitOfWork
            )
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }
        #endregion
        public bool Delete(int id)
        {
            try
            {
                _unitOfWork.Propietario.Delete(id);
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                return false;
            }

            return true;
        }

        public PropietarioDTO GetById(int id)
        {
            var model = _unitOfWork.Propietario.GetById(id);
            return _mapper.Map<PropietarioDTO>(model);
        }

        public List<PropietarioDTO> GetAll()
        {
            var model = _unitOfWork.Propietario.GetAll();
            return _mapper.Map<List<PropietarioDTO>>(model);
        }

        public List<PropietarioDTO> GetAllBy(int id)
        {
            var model = _unitOfWork.Propietario.GetAllBy(x => x.Id == id);
            return _mapper.Map<List<PropietarioDTO>>(model);
        }



        public bool Save(PropietarioDTO model)
        {
            try
            {
                var entitie = _mapper.Map<Propietario>(model);
                _unitOfWork.Propietario.Save(entitie);
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                return false;
            }

            return true;
        }

        public bool Update(int id)
        {
            try
            {
                _unitOfWork.Propietario.Update(id);
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                return false;
            }

            return true;
        }
    }

}
