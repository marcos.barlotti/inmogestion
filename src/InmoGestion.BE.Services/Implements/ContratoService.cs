﻿using AutoMapper;
using InmoGestion.BE.Models.DTOs;
using InmoGestion.BE.Models.Entities;
using InmoGestion.BE.Services.Interface;
using InmoGestion.BE.Repositories.Interface;

namespace InmoGestion.BE.Services.Implements
{
    public class ContratoService : IContratoService
    {
        #region Dependencias
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;
        #endregion

        #region Constructor
        public ContratoService(
            IMapper mapper,
            IUnitOfWork unitOfWork
            )
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }
        #endregion
        public bool Delete(int id)
        {
            try
            {
                _unitOfWork.Contrato.Delete(id);
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                return false;
            }

            return true;
        }

        public ContratoDTO GetById(int id)
        {
            var model = _unitOfWork.Contrato.GetById(id);
            return _mapper.Map<ContratoDTO>(model);
        }

        public List<ContratoDTO> GetAll()
        {
            var model = _unitOfWork.Contrato.GetAll();
            return _mapper.Map<List<ContratoDTO>>(model);
        }

        public List<ContratoDTO> GetAllBy(int id)
        {
            var model = _unitOfWork.Contrato.GetAllBy(x => x.Id == id);
            return _mapper.Map<List<ContratoDTO>>(model);
        }



        public bool Save(ContratoDTO model)
        {
            try
            {
                var entitie = _mapper.Map<Contrato>(model);
                _unitOfWork.Contrato.Save(entitie);
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                return false;
            }

            return true;
        }

        public bool Update(int id)
        {
            try
            {
                _unitOfWork.Contrato.Update(id);
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                return false;
            }

            return true;
        }
    }
}
