﻿using AutoMapper;
using InmoGestion.BE.Models.DTOs;
using InmoGestion.BE.Models.Entities;
using InmoGestion.BE.Services.Interface;
using InmoGestion.BE.Repositories.Interface;

namespace InmoGestion.BE.Services.Implements
{
    public class DireccionService : IDireccionService
    {
        #region Dependencias
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;
        #endregion

        #region Constructor
        public DireccionService(
            IMapper mapper,
            IUnitOfWork unitOfWork
            )
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }
        #endregion
        public bool Delete(int id)
        {
            try
            {
                _unitOfWork.Direccion.Delete(id);
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                return false;
            }

            return true;
        }

        public DireccionDTO GetById(int id)
        {
            var model = _unitOfWork.Direccion.GetById(id);
            return _mapper.Map<DireccionDTO>(model);
        }

        public List<DireccionDTO> GetAll()
        {
            var model = _unitOfWork.Direccion.GetAll();
            return _mapper.Map<List<DireccionDTO>>(model);
        }

        public List<DireccionDTO> GetAllBy(int id)
        {
            var model = _unitOfWork.Direccion.GetAllBy(x => x.Id == id);
            return _mapper.Map<List<DireccionDTO>>(model);
        }



        public bool Save(DireccionDTO model)
        {
            try
            {
                var entitie = _mapper.Map<Direccion>(model);
                _unitOfWork.Direccion.Save(entitie);
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                return false;
            }

            return true;
        }

        public bool Update(int id)
        {
            try
            {
                _unitOfWork.Direccion.Update(id);
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                return false;
            }

            return true;
        }
    }
}
