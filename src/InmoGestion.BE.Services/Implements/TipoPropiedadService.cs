﻿using AutoMapper;
using InmoGestion.BE.Models.DTOs;
using InmoGestion.BE.Models.Entities;
using InmoGestion.BE.Repositories.Interface;
using InmoGestion.BE.Services.Interface;

namespace InmoGestion.BE.Services.Implements
{
    public class TipoPropiedadService : ITipoPropiedadService
    {
        #region Dependencias
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;
        #endregion

        #region Constructor
        public TipoPropiedadService(
            IMapper mapper,
            IUnitOfWork unitOfWork
            )
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }
        #endregion
        public bool Delete(int id)
        {
            try
            {
                _unitOfWork.TipoPropiedad.Delete(id);
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                return false;
            }

            return true;
        }

        public TipoPropiedadDTO GetById(int id)
        {
            var model = _unitOfWork.TipoPropiedad.GetById(id);
            return _mapper.Map<TipoPropiedadDTO>(model);
        }

        public List<TipoPropiedadDTO> GetAll()
        {
            var model = _unitOfWork.TipoPropiedad.GetAll();
            return _mapper.Map<List<TipoPropiedadDTO>>(model);
        }

        public List<TipoPropiedadDTO> GetAllBy(int id)
        {
            var model = _unitOfWork.TipoPropiedad.GetAllBy(x => x.Id == id);
            return _mapper.Map<List<TipoPropiedadDTO>>(model);
        }



        public bool Save(TipoPropiedadDTO model)
        {
            try
            {
                var entitie = _mapper.Map<TipoPropiedad>(model);
                _unitOfWork.TipoPropiedad.Save(entitie);
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                return false;
            }

            return true;
        }

        public bool Update(int id)
        {
            try
            {
                _unitOfWork.TipoPropiedad.Update(id);
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                return false;
            }

            return true;
        }
    }

}
