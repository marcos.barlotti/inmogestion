﻿using AutoMapper;
using InmoGestion.BE.Models.DTOs;
using InmoGestion.BE.Models.Entities;
using InmoGestion.BE.Services.Interface;
using InmoGestion.BE.Repositories.Interface;

namespace InmoGestion.BE.Services.Implements
{
    public class CatastroService : ICatastroService
    {
        #region Dependencias
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;
        #endregion

        #region Constructor
        public CatastroService(
            IMapper mapper,
            IUnitOfWork unitOfWork
            )
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }
        #endregion
        public bool Delete(int id)
        {
            try
            {
                _unitOfWork.Catastro.Delete(id);
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                return false;
            }

            return true;
        }

        public CatastroDTO GetById(int id)
        {
            var model = _unitOfWork.Catastro.GetById(id);
            return _mapper.Map<CatastroDTO>(model);
        }

        public List<CatastroDTO> GetAll()
        {
            var model = _unitOfWork.Catastro.GetAll();
            return _mapper.Map<List<CatastroDTO>>(model);
        }

        public List<CatastroDTO> GetAllBy(int id)
        {
            var model = _unitOfWork.Catastro.GetAllBy(x => x.Id == id);
            return _mapper.Map<List<CatastroDTO>>(model);
        }



        public bool Save(CatastroDTO model)
        {
            try
            {
                var entitie = _mapper.Map<Catastro>(model);
                _unitOfWork.Catastro.Save(entitie);
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                return false;
            }

            return true;
        }

        public bool Update(int id)
        {
            try
            {
                _unitOfWork.Catastro.Update(id);
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                return false;
            }

            return true;
        }
    }

}
