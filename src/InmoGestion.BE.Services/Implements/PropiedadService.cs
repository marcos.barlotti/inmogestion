﻿using AutoMapper;
using InmoGestion.BE.Models.DTOs;
using InmoGestion.BE.Models.Entities;
using InmoGestion.BE.Services.Interface;
using InmoGestion.BE.Repositories.Interface;

namespace InmoGestion.BE.Services.Implements
{
    public class PropiedadService : IPropiedadService
    {
        #region Dependencias
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;
        #endregion

        #region Constructor
        public PropiedadService(
            IMapper mapper,
            IUnitOfWork unitOfWork
            )
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }
        #endregion
        public bool Delete(int id)
        {
            try
            {
                _unitOfWork.Propiedad.Delete(id);
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                return false;
            }

            return true;
        }

        public PropiedadDTO GetById(int id)
        {
            var model = _unitOfWork.Propiedad.GetById(id);
            return _mapper.Map<PropiedadDTO>(model);
        }

        public List<PropiedadDTO> GetAll()
        {
            var model = _unitOfWork.Propiedad.GetAll();
            return _mapper.Map<List<PropiedadDTO>>(model);
        }

        public List<PropiedadDTO> GetAllBy(int id)
        {
            var model = _unitOfWork.Propiedad.GetAllBy(x => x.Id == id);
            return _mapper.Map<List<PropiedadDTO>>(model);
        }



        public bool Save(PropiedadDTO model)
        {
            try
            {
                var entitie = _mapper.Map<Propiedad>(model);
                _unitOfWork.Propiedad.Save(entitie);
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                return false;
            }

            return true;
        }

        public bool Update(int id)
        {
            try
            {
                _unitOfWork.Propiedad.Update(id);
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                return false;
            }

            return true;
        }
    }
}
