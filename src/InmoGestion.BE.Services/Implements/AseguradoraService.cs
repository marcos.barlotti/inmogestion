﻿using AutoMapper;
using InmoGestion.BE.Models.DTOs;
using InmoGestion.BE.Models.Entities;
using InmoGestion.BE.Services.Interface;
using InmoGestion.BE.Repositories.Interface;

namespace InmoGestion.BE.Services.Implements
{
    public class AseguradoraService : IAseguradoraService
    {
        #region Dependencias
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;
        #endregion

        #region Constructor
        public AseguradoraService(
            IMapper mapper,
            IUnitOfWork unitOfWork
            )
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }
        #endregion
        public bool Delete(int id)
        {
            try
            {
                _unitOfWork.Aseguradora.Delete(id);
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                return false;
            }

            return true;
        }

        public AseguradoraDTO GetById(int id)
        {
            var model = _unitOfWork.Aseguradora.GetById(id);
            return _mapper.Map<AseguradoraDTO>(model);
        }

        public List<AseguradoraDTO> GetAll()
        {
            var model = _unitOfWork.Aseguradora.GetAll();
            return _mapper.Map<List<AseguradoraDTO>>(model);
        }

        public List<AseguradoraDTO> GetAllBy(int id)
        {
            var model = _unitOfWork.Aseguradora.GetAllBy(x => x.Id == id);
            return _mapper.Map<List<AseguradoraDTO>>(model);
        }



        public bool Save(AseguradoraDTO model)
        {
            try
            {
                var entitie = _mapper.Map<Aseguradora>(model);
                _unitOfWork.Aseguradora.Save(entitie);
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                return false;
            }

            return true;
        }

        public bool Update(int id)
        {
            try
            {
                _unitOfWork.Aseguradora.Update(id);
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                return false;
            }

            return true;
        }
    }
}
