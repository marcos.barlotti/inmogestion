﻿using AutoMapper;
using InmoGestion.BE.Models.DTOs;
using InmoGestion.BE.Models.Entities;
using InmoGestion.BE.Services.Interface;
using InmoGestion.BE.Repositories.Interface;

namespace InmoGestion.BE.Services.Implements
{
    public class InquilinoService : IInquilinoService
    {
        #region Dependencias
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;
        #endregion

        #region Constructor
        public InquilinoService(
            IMapper mapper,
            IUnitOfWork unitOfWork
            )
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }
        #endregion
        public bool Delete(int id)
        {
            try
            {
                _unitOfWork.Inquilino.Delete(id);
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                return false;
            }

            return true;
        }

        public InquilinoDTO GetById(int id)
        {
            var model = _unitOfWork.Inquilino.GetById(id);
            return _mapper.Map<InquilinoDTO>(model);
        }

        public List<InquilinoDTO> GetAll()
        {
            var model = _unitOfWork.Inquilino.GetAll();
            return _mapper.Map<List<InquilinoDTO>>(model);
        }

        public List<InquilinoDTO> GetAllBy(int id)
        {
            var model = _unitOfWork.Inquilino.GetAllBy(x => x.Id == id);
            return _mapper.Map<List<InquilinoDTO>>(model);
        }



        public bool Save(InquilinoDTO model)
        {
            try
            {
                var entitie = _mapper.Map<Inquilino>(model);
                _unitOfWork.Inquilino.Save(entitie);
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                return false;
            }

            return true;
        }

        public bool Update(int id)
        {
            try
            {
                _unitOfWork.Inquilino.Update(id);
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                return false;
            }

            return true;
        }
    }


}
