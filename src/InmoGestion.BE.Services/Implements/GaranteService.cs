﻿using AutoMapper;
using InmoGestion.BE.Models.DTOs;
using InmoGestion.BE.Models.Entities;
using InmoGestion.BE.Services.Interface;
using InmoGestion.BE.Repositories.Interface;

namespace InmoGestion.BE.Services.Implements
{
    public class GaranteService : IGaranteService
    {
        #region Dependencias
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;
        #endregion

        #region Constructor
        public GaranteService(
            IMapper mapper,
            IUnitOfWork unitOfWork
            )
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }
        #endregion
        public bool Delete(int id)
        {
            try
            {
                _unitOfWork.Garante.Delete(id);
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                return false;
            }

            return true;
        }

        public GaranteDTO GetById(int id)
        {
            var model = _unitOfWork.Garante.GetById(id);
            return _mapper.Map<GaranteDTO>(model);
        }

        public List<GaranteDTO> GetAll()
        {
            var model = _unitOfWork.Garante.GetAll();
            return _mapper.Map<List<GaranteDTO>>(model);
        }

        public List<GaranteDTO> GetAllBy(int id)
        {
            var model = _unitOfWork.Garante.GetAllBy(x => x.Id == id);
            return _mapper.Map<List<GaranteDTO>>(model);
        }



        public bool Save(GaranteDTO model)
        {
            try
            {
                var entitie = _mapper.Map<Garante>(model);
                _unitOfWork.Garante.Save(entitie);
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                return false;
            }

            return true;
        }

        public bool Update(int id)
        {
            try
            {
                _unitOfWork.Garante.Update(id);
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                return false;
            }

            return true;
        }
    }
}
