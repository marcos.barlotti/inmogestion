﻿using AutoMapper;
using InmoGestion.BE.Models.DTOs;
using InmoGestion.BE.Models.Entities;
using InmoGestion.BE.Services.Interface;
using InmoGestion.BE.Repositories.Interface;

namespace InmoGestion.BE.Services.Implements
{
    public class TipoGarantiaService : ITipoGarantiaService
    {
        #region Dependencias
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;
        #endregion

        #region Constructor
        public TipoGarantiaService(
            IMapper mapper,
            IUnitOfWork unitOfWork
            )
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }
        #endregion
        public bool Delete(int id)
        {
            try
            {
                _unitOfWork.TipoGarantia.Delete(id);
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                return false;
            }

            return true;
        }

        public TipoGarantiaDTO GetById(int id)
        {
            var model = _unitOfWork.TipoGarantia.GetById(id);
            return _mapper.Map<TipoGarantiaDTO>(model);
        }

        public List<TipoGarantiaDTO> GetAll()
        {
            var model = _unitOfWork.TipoGarantia.GetAll();
            return _mapper.Map<List<TipoGarantiaDTO>>(model);
        }

        public List<TipoGarantiaDTO> GetAllBy(int id)
        {
            var model = _unitOfWork.TipoGarantia.GetAllBy(x => x.Id == id);
            return _mapper.Map<List<TipoGarantiaDTO>>(model);
        }



        public bool Save(TipoGarantiaDTO model)
        {
            try
            {
                var entitie = _mapper.Map<TipoGarantia>(model);
                _unitOfWork.TipoGarantia.Save(entitie);
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                return false;
            }

            return true;
        }

        public bool Update(int id)
        {
            try
            {
                _unitOfWork.TipoGarantia.Update(id);
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                return false;
            }

            return true;
        }
    }
}
