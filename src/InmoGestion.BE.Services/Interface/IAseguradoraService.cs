﻿using InmoGestion.BE.Models.DTOs;

namespace InmoGestion.BE.Services.Interface
{
    public interface IAseguradoraService
    {
        public bool Save(AseguradoraDTO model);
        public AseguradoraDTO GetById(int id);
        public List<AseguradoraDTO> GetAll();
        public List<AseguradoraDTO> GetAllBy(int id);
        public bool Update(int id);
        public bool Delete(int id);
    }
}