﻿using InmoGestion.BE.Models.DTOs;
using InmoGestion.BE.Models.DTOs;

namespace InmoGestion.BE.Services.Interface
{
    public interface IPropietarioService
    {
        public bool Save(PropietarioDTO model);
        public PropietarioDTO GetById(int id);
        public List<PropietarioDTO> GetAll();
        public List<PropietarioDTO> GetAllBy(int id);
        public bool Update(int id);
        public bool Delete(int id);
    }
}