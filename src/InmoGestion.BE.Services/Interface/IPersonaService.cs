﻿using InmoGestion.BE.Models.DTOs;

namespace InmoGestion.BE.Services.Interface
{
    public interface IPersonaService
    {
        public bool Save(PersonaDTO model);
        public PersonaDTO GetById(int id);
        public List<PersonaDTO> GetAll();
        public List<PersonaDTO> GetAllBy(int id);
        public bool Update(int id);
        public bool Delete(int id);
    }
}