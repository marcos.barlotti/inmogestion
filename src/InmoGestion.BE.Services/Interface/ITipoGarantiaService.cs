﻿using InmoGestion.BE.Models.DTOs;

namespace InmoGestion.BE.Services.Interface
{
    public interface ITipoGarantiaService
    {
        public bool Save(TipoGarantiaDTO model);
        public TipoGarantiaDTO GetById(int id);
        public List<TipoGarantiaDTO> GetAll();
        public List<TipoGarantiaDTO> GetAllBy(int id);
        public bool Update(int id);
        public bool Delete(int id);
    }
}