﻿using InmoGestion.BE.Models.DTOs;

namespace InmoGestion.BE.Services.Interface
{
    public interface IDireccionService
    {
        public bool Save(DireccionDTO model);
        public DireccionDTO GetById(int id);
        public List<DireccionDTO> GetAll();
        public List<DireccionDTO> GetAllBy(int id);
        public bool Update(int id);
        public bool Delete(int id);
    }
}