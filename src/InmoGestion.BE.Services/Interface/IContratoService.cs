﻿using InmoGestion.BE.Models.DTOs;

namespace InmoGestion.BE.Services.Interface
{
    public interface IContratoService
    {
        public bool Save(ContratoDTO model);
        public ContratoDTO GetById(int id);
        public List<ContratoDTO> GetAll();
        public List<ContratoDTO> GetAllBy(int id);
        public bool Update(int id);
        public bool Delete(int id);
    }
}