﻿using InmoGestion.BE.Models.DTOs;

namespace InmoGestion.BE.Services.Interface
{
    public interface IInquilinoService
    {
        public bool Save(InquilinoDTO model);
        public InquilinoDTO GetById(int id);
        public List<InquilinoDTO> GetAll();
        public List<InquilinoDTO> GetAllBy(int id);
        public bool Update(int id);
        public bool Delete(int id);
    }
}