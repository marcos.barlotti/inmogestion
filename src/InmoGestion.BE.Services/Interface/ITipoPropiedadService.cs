﻿using InmoGestion.BE.Models.DTOs;

namespace InmoGestion.BE.Services.Interface
{
    public interface ITipoPropiedadService
    {
        public bool Save(TipoPropiedadDTO model);
        public TipoPropiedadDTO GetById(int id);
        public List<TipoPropiedadDTO> GetAll();
        public List<TipoPropiedadDTO> GetAllBy(int id);
        public bool Update(int id);
        public bool Delete(int id);
    }
}