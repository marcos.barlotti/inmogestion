﻿using InmoGestion.BE.Models.DTOs;

namespace InmoGestion.BE.Services.Interface
{
    public interface ICatastroService
    {
        public bool Save(CatastroDTO model);
        public CatastroDTO GetById(int id);
        public List<CatastroDTO> GetAll();
        public List<CatastroDTO> GetAllBy(int id);
        public bool Update(int id);
        public bool Delete(int id);
    }
}