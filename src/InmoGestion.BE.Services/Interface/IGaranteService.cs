﻿using InmoGestion.BE.Models.DTOs;

namespace InmoGestion.BE.Services.Interface
{
    public interface IGaranteService
    {
        public bool Save(GaranteDTO model);
        public GaranteDTO GetById(int id);
        public List<GaranteDTO> GetAll();
        public List<GaranteDTO> GetAllBy(int id);
        public bool Update(int id);
        public bool Delete(int id);
    }
}