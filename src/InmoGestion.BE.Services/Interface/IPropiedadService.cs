﻿using InmoGestion.BE.Models.DTOs;

namespace InmoGestion.BE.Services.Interface
{
    public interface IPropiedadService
    {
        public bool Save(PropiedadDTO model);
        public PropiedadDTO GetById(int id);
        public List<PropiedadDTO> GetAll();
        public List<PropiedadDTO> GetAllBy(int id);
        public bool Update(int id);
        public bool Delete(int id);
        //public IEnumerable<PropiedadDTO> GetByUbicacion(DireccionDTO filtro);
    }
}