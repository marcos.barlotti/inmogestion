﻿using InmoGestion.BE.Services.Implements;
using InmoGestion.BE.Services.Interface;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InmoGestion.BE.Services.Support
{
    public static class ServicesStartup
    {
        /// <summary>
        /// Metodo extensivo para la configuracion de los services
        /// </summary>
        /// <param name="services"></param>
        /// <param name="Configuration"></param>
        /// <returns></returns>

        public static IServiceCollection AddInternalServices(this IServiceCollection services)
        {
            services.AddTransient<IAseguradoraService, AseguradoraService>();
            services.AddTransient<ICatastroService, CatastroService>();
            services.AddTransient<IContratoService, ContratoService>();
            services.AddTransient<IDireccionService, DireccionService>();
            services.AddTransient<IGaranteService, GaranteService>();
            services.AddTransient<IInquilinoService, InquilinoService>();
            services.AddTransient<IPersonaService, PersonaService>();
            services.AddTransient<IPropiedadService, PropiedadService>();
            services.AddTransient<IPropietarioService, PropietarioService>();
            services.AddTransient<ITipoGarantiaService, TipoGarantiaService>();
            services.AddTransient<ITipoPropiedadService, TipoPropiedadService>();
            return services;
        }
    }
}
