﻿using InmoGestion.BE.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using InmoGestion.BE.DataAccess.ModelsBuilder;

namespace InmoGestion.BE.DataAccess
{
    public class InmoGestionDbContext : DbContext
    {
        public InmoGestionDbContext(DbContextOptions<InmoGestionDbContext> options) : base(options)
        {

        }
        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfigurationsFromAssembly(typeof(PropiedadEntityTypeConfiguration).Assembly);
            base.OnModelCreating(builder);
        }

        public DbSet<Propiedad> Propiedad { get; set; }

    }
}
