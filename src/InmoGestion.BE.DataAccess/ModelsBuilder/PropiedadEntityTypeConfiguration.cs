﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;
using InmoGestion.BE.Models.Entities;

namespace InmoGestion.BE.DataAccess.ModelsBuilder
{
    public class PropiedadEntityTypeConfiguration : IEntityTypeConfiguration<Propiedad>
    {
        public void Configure(EntityTypeBuilder<Propiedad> builder)
        {
            builder
                .ToTable("")
                .HasKey(x => new { x.Id });
        }
    }
}
