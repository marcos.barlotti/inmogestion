﻿using InmoGestion.BE.DataAccess;
using InmoGestion.BE.Repositories.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InmoGestion.BE.Repositories.UoW
{
    public class UnitOfWork : IUnitOfWork
    {
        #region Dependencias
        private readonly InmoGestionDbContext _context;
        public IAseguradoraRepository Aseguradora { get; }
        public ICatastroRepository Catastro { get; }
        public IContratoRepository Contrato { get; }
        public IDireccionRepository Direccion { get; }
        public IGaranteRepository Garante { get; }
        public IInquilinoRepository Inquilino { get; }
        public IPersonaRepository Persona { get; }
        public IPropiedadRepository Propiedad { get; }
        public IPropietarioRepository Propietario { get; }
        public ITipoGarantiaRepository TipoGarantia { get; }
        public ITipoPropiedadRepository TipoPropiedad { get; }
        #endregion

        #region Constructor
        public UnitOfWork(
            InmoGestionDbContext context,
            IPropiedadRepository propiedadRepository,
            IAseguradoraRepository aseguradora,
            ICatastroRepository catastro,
            IContratoRepository contrato,
            IDireccionRepository direccion,
            IGaranteRepository garante,
            IInquilinoRepository inquilino,
            IPersonaRepository persona,
            IPropietarioRepository propietario,
            ITipoGarantiaRepository tipoGarantia,
            ITipoPropiedadRepository tipoPropiedad)
        {
            this._context = context;
            this.Propiedad = propiedadRepository;
            this.Aseguradora = aseguradora;
            this.Catastro = catastro;
            this.Contrato = contrato;
            this.Direccion = direccion;
            this.Garante = garante;
            this.Inquilino = inquilino;
            this.Persona = persona;
            this.Propietario = propietario;
            this.TipoGarantia = tipoGarantia;
            this.TipoPropiedad = tipoPropiedad;
        }
        #endregion

        public async Task SaveChanges()
        {
            await _context.SaveChangesAsync();
        }
        public void ChangeTrackerClear()
        {
            _context.ChangeTracker.Clear();
        }
    }
}
