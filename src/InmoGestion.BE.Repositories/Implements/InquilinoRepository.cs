﻿using InmoGestion.BE.Models.Entities;
using InmoGestion.BE.DataAccess;
using InmoGestion.BE.Repositories.Interface;
using InmoGestion.BE.Repositories.Implements.Generic;

namespace InmoGestion.BE.Repositories.Implements
{
    public class InquilinoRepository : GenericRepository<Inquilino>, IInquilinoRepository
    {
        public InquilinoRepository(InmoGestionDbContext context) : base(context)
        {
        }

    }
}
