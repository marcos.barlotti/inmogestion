﻿using InmoGestion.BE.Models.Entities;
using InmoGestion.BE.DataAccess;
using InmoGestion.BE.Repositories.Interface;
using InmoGestion.BE.Repositories.Implements.Generic;

namespace InmoGestion.BE.Repositories.Implements
{
    public class PropietarioRepository : GenericRepository<Propietario>, IPropietarioRepository
    {
        public PropietarioRepository(InmoGestionDbContext context) : base(context)
        {
        }

    }
}
