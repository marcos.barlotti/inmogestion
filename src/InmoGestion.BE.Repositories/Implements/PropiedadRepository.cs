﻿using InmoGestion.BE.Models.Entities;
using InmoGestion.BE.DataAccess;
using InmoGestion.BE.Repositories.Interface;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InmoGestion.BE.Repositories.Implements.Generic;

namespace InmoGestion.BE.Repositories.Implements
{
    public class PropiedadRepository : GenericRepository<Propiedad>, IPropiedadRepository
    {
        public PropiedadRepository(InmoGestionDbContext context) : base(context)
        {
        }
    }
}
