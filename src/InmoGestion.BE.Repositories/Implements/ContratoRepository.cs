﻿using InmoGestion.BE.Models.Entities;
using InmoGestion.BE.DataAccess;
using InmoGestion.BE.Repositories.Interface;
using InmoGestion.BE.Repositories.Implements.Generic;

namespace InmoGestion.BE.Repositories.Implements
{
    public class ContratoRepository : GenericRepository<Contrato>, IContratoRepository
    {
        public ContratoRepository(InmoGestionDbContext context) : base(context)
        {
        }

    }

}
