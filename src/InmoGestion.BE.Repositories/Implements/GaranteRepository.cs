﻿using InmoGestion.BE.Models.Entities;
using InmoGestion.BE.DataAccess;
using InmoGestion.BE.Repositories.Interface;
using InmoGestion.BE.Repositories.Implements.Generic;

namespace InmoGestion.BE.Repositories.Implements
{
    public class GaranteRepository : GenericRepository<Garante>, IGaranteRepository
    {
        public GaranteRepository(InmoGestionDbContext context) : base(context)
        {
        }

    }
}
