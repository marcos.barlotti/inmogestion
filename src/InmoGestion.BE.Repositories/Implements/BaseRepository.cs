﻿using InmoGestion.BE.DataAccess;
using InmoGestion.BE.Repositories.Interface;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InmoGestion.BE.Repositories.Implements
{
    public class BaseRepository<TEntity> : IRepository<TEntity>
        where TEntity : class
    {
        #region Dependencias
        protected readonly InmoGestionDbContext _dbContext;
        protected readonly DbSet<TEntity> _dbSet;
        #endregion

        #region Constructor
        public BaseRepository(
            InmoGestionDbContext context
            )
        {
            _dbContext = context;
            _dbSet = _dbContext.Set<TEntity>();
        }

        public IEnumerable<TEntity> GetAll()
        {
            return _dbSet.AsEnumerable();
        }
        public IQueryable<TEntity> GetQueryable()
        {
            return _dbSet.AsNoTracking();
        }
        #endregion
    }
}
