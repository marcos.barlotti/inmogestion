﻿using InmoGestion.BE.DataAccess;
using InmoGestion.BE.Repositories.Interface.Generic;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace InmoGestion.BE.Repositories.Implements.Generic
{
    public class GenericRepository<TEntity> : IDisposable, IGenericRepository<TEntity> where TEntity : class
    {
        protected readonly InmoGestionDbContext _context;
        internal DbSet<TEntity> dbSet;

        public GenericRepository(InmoGestionDbContext context)
        {
            this._context = context;
            this.dbSet = context.Set<TEntity>();
        }


        public virtual async Task<IEnumerable<TEntity>> GetAll(params Expression<Func<TEntity, object>>[] includes)
        {
            IQueryable<TEntity> set = dbSet;

            foreach (Expression<Func<TEntity, object>> include in includes)
                set = set.Include(include);

            return await set.ToListAsync();
        }

        public async Task<IEnumerable<TEntity>> GetAllBy(Expression<Func<TEntity, bool>> whereConditions = null, Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null, params Expression<Func<TEntity, object>>[] includes)
        {
            IQueryable<TEntity> set = dbSet;

            if (whereConditions != null)
            {
                set = set.Where(whereConditions);
            }

            foreach (Expression<Func<TEntity, object>> include in includes)
                set = set.Include(include);

            if (orderBy != null)
                return await orderBy(set).ToListAsync();

            return await set.AsNoTracking().ToListAsync();
        }
        public virtual async Task Delete(int id)
        {
            var entity = await GetById(id);
            _context.Set<TEntity>().Remove(entity);
            await _context.SaveChangesAsync();
        }

        public virtual async Task<TEntity> GetById(int id)
        {
            return await _context.Set<TEntity>().FindAsync(id);
        }


        public async Task<TEntity> Save(TEntity entity)
        {
            try
            {
                _context.Set<TEntity>().Add(entity);
                await _context.SaveChangesAsync();
                return entity;
            }
            catch (Exception ex)
            {
                var message = ex.Message;
                throw;
            }

        }

        public virtual async Task<TEntity> GetById(int id, params Expression<Func<TEntity, object>>[] includes)
        {
            IQueryable<TEntity> set = dbSet;

            foreach (Expression<Func<TEntity, object>> include in includes)
                set = set.Include(include);

            return await set.FirstAsync();
        }

        public async Task<TEntity> Update(int id)
        {
            var entity = await GetById(id);
            _context.Entry(entity).State = EntityState.Modified;
            await _context.SaveChangesAsync();
            return entity;
        }


        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
