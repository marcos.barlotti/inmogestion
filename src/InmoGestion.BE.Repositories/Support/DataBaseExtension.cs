﻿using InmoGestion.BE.Repositories.Implements;
using InmoGestion.BE.Repositories.UoW;
using InmoGestion.BE.DataAccess;
using InmoGestion.BE.Repositories.Interface;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InmoGestion.BE.Repositories.Support
{
    public static class DataBaseExtension
    {
        /// <summary>
        /// Metodo extensivo para la configuracion de la base de datos
        /// </summary>
        /// <param name="services"></param>
        /// <param name="Configuration"></param>
        /// <returns></returns>
        /// 
        public static IServiceCollection AddRepositoryInmoGestion(this IServiceCollection services, IConfiguration Configuration)
        {
            #region Unit Of Work
            services.AddTransient<IUnitOfWork, UnitOfWork>();
            #endregion

            #region DbContext
            string conn_InmoGestion = Configuration.GetConnectionString("InmoGestion");
            services.AddDbContextPool<InmoGestionDbContext>(options => options.UseSqlServer(conn_InmoGestion));
            #endregion

            #region Repositories
            services.AddTransient<IPropiedadRepository, PropiedadRepository>();
            #endregion
            return services;
        }
    }
}
