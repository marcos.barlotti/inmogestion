﻿using InmoGestion.BE.Models.Entities;
using InmoGestion.BE.Repositories.Interface.Generic;

namespace InmoGestion.BE.Repositories.Interface
{
    public interface IPersonaRepository : IGenericRepository<Persona>
    {

    }
}


