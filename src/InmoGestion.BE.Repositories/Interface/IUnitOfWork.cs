﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InmoGestion.BE.Repositories.Interface
{
    public interface IUnitOfWork
    {
        IAseguradoraRepository Aseguradora { get; }
        ICatastroRepository Catastro { get; }
        IContratoRepository Contrato { get; }
        IDireccionRepository Direccion { get; }
        IGaranteRepository Garante { get; }
        IInquilinoRepository Inquilino { get; }
        IPersonaRepository Persona { get; }
        IPropiedadRepository Propiedad { get; }
        IPropietarioRepository Propietario { get; }
        ITipoGarantiaRepository TipoGarantia { get; }
        ITipoPropiedadRepository TipoPropiedad { get; }
        Task SaveChanges();
        void ChangeTrackerClear();
    }
}
