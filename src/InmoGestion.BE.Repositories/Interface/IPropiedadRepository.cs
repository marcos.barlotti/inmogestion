﻿using InmoGestion.BE.Models.Entities;
using InmoGestion.BE.Repositories.Interface.Generic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InmoGestion.BE.Repositories.Interface
{
    public interface IPropiedadRepository : IGenericRepository<Propiedad>
    {
       
        //public IEnumerable<Propiedad> GetByUbicacion(DireccionDTO filtro);
    }
}


