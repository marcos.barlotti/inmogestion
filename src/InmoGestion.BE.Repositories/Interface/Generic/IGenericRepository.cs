﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace InmoGestion.BE.Repositories.Interface.Generic
{
    public interface IGenericRepository<TEntity> where TEntity : class
    {
        Task<IEnumerable<TEntity>> GetAll(params Expression<Func<TEntity, object>>[] includes);
        Task<IEnumerable<TEntity>> GetAllBy(Expression<Func<TEntity, bool>> whereConditions = null,
                                           Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
                                           params Expression<Func<TEntity, object>>[] includes);

        Task<TEntity> GetById(int id, params Expression<Func<TEntity, object>>[] includes);
        Task<TEntity> Save(TEntity entity);
        Task<TEntity> Update(int id);
        Task Delete(int id);
    }
}
