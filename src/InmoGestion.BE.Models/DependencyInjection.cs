﻿using AutoMapper;
using InmoGestion.BE.Models.Profiles;
using Microsoft.Extensions.DependencyInjection;

namespace InmoGestion.BE.Models
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddMaps(this IServiceCollection services)
        {
            var mapperConfig = new MapperConfiguration(m =>
            {
                m.AddProfile(new AseguradoraProfile());
                m.AddProfile(new CatastroProfile());
                m.AddProfile(new ContratoProfile());
                m.AddProfile(new DireccionProfile());
                m.AddProfile(new GaranteProfile());
                m.AddProfile(new InquilinoProfile());
                m.AddProfile(new PersonaProfile());
                m.AddProfile(new PropiedadProfile());
                m.AddProfile(new PropietarioProfile());
                m.AddProfile(new TipoGarantiaProfile());
                m.AddProfile(new TipoPropiedadProfile());
            });
            IMapper mapper = mapperConfig.CreateMapper();
            services.AddSingleton(mapper);

            return services;
        }
    }
}
