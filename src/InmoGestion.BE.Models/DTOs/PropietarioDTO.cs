﻿using InmoGestion.BE.Models.Entities;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using InmoGestion.BE.Models.DTOs;

namespace InmoGestion.BE.Models.DTOs
{
    public class PropietarioDTO
    {
       
        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Display(Name="Id")]
        public int Id { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Display(Name="Id Persona")]
        public int IdPersona { get; set; }
        public PersonaDTO Persona { get; set; }
    }
}