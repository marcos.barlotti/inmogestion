﻿using InmoGestion.BE.Models.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InmoGestion.BE.Models.DTOs
{
    public class GaranteDTO
    {
       
        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Display(Name = "Id")]
        public int Id { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Display(Name = "Id Inquilino")]
        public int IdInquilino { get; set; }
        public InquilinoDTO Inquilino { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Display(Name = "Id Persona")]
        public int IdPersona { get; set; }
        public PersonaDTO Persona { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Display(Name = "Id Tipo Garantia")]
        public int IdTipoGarantia { get; set; }
        public TipoGarantiaDTO TipoGarantia { get; set; }
    }
}
