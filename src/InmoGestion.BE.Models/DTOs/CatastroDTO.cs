﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace InmoGestion.BE.Models.DTOs
{
    public class CatastroDTO
    {
        
        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Display(Name="Id")]
        public int Id { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Display(Name="Seccion")]
        public int Seccion { get; set; }

        [Display(Name="Manzana")]
        [StringLength(100)]
        [Required(ErrorMessage = "El campo {0} es requerido")]
        [DataType(DataType.Text)]
        public string Manzana { get; set; }

        [Display(Name="Parcela")]
        [StringLength(100)]
        [Required(ErrorMessage = "El campo {0} es requerido")]
        [DataType(DataType.Text)]
        public string Parcela { get; set; }

        [Display(Name="Nomenclatura")]
        [StringLength(100)]
        [Required(ErrorMessage = "El campo {0} es requerido")]
        [DataType(DataType.Text)]
        public string Nomenclatura { get; set; }
    }
}