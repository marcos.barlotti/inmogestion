﻿using InmoGestion.BE.Models.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InmoGestion.BE.Models.DTOs
{
    public class AseguradoraDTO
    {
        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Display(Name="Id")]
        public int Id { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Display(Name="Id_Inquilino")]
        public int IdInquilino { get; set; }
        public InquilinoDTO Inquilino { get; set; }


        [Display(Name="Razon_Social")]
        [StringLength(100)]
        [Required(ErrorMessage = "El campo {0} es requerido")]
        [DataType(DataType.Text)]
        public string RazonSocial { get; set; }
    }
}
