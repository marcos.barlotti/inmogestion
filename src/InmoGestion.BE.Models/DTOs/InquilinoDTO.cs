﻿using InmoGestion.BE.Models.Entities;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace InmoGestion.BE.Models.DTOs
{
    public class InquilinoDTO
    {
        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Display(Name="Id")]
        public int Id { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Display(Name="Id Contrato")]
        public int IdContrato { get; set; }
        public ContratoDTO Contrato { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Display(Name="Id Persona")]
        public int IdPersona { get; set; }
        public PersonaDTO Persona { get; set; }
    }
}