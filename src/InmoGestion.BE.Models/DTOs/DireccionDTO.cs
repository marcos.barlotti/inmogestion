﻿using InmoGestion.BE.Models.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InmoGestion.BE.Models.DTOs
{
    public class DireccionDTO
    {
        
        [Display(Name="Id")]
        public int Id { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Display(Name="Id_Catastro")]
        public int IdCatastro { get; set; }
        public CatastroDTO Catastro { get; set; }

        [Display(Name="Calle")]
        [StringLength(100)]
        [Required(ErrorMessage = "El campo {0} es requerido")]
        [DataType(DataType.Text)]
        public string Calle { get; set; }

        [Display(Name="Numero")]
        [Required(ErrorMessage = "El campo {0} es requerido")]
        public int Numero { get; set; }

        [Display(Name="Piso")]
        [Required(ErrorMessage = "El campo {0} es requerido")]
        public int Piso { get; set; }

        [Display(Name="Departamento")]
        [StringLength(100)]
        [Required(ErrorMessage = "El campo {0} es requerido")]
        [DataType(DataType.Text)]
        public string Departamento { get; set; }
    }
}
