﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace InmoGestion.BE.Models.DTOs
{
    public class TipoGarantiaDTO
    {
       
        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Display(Name="Id")]
        public int Id { get; set; }

        [Display(Name="Descripcion")]
        [StringLength(100)]
        [Required(ErrorMessage = "El campo {0} es requerido")]
        [DataType(DataType.Text)]
        public string Descripcion { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Display(Name="Recibo")]
        public float? Recibo { get; set; }
    }
}