﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace InmoGestion.BE.Models.DTOs
{
    public class TipoPropiedadDTO
    {
        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Column("Id")]
        public int Id { get; set; }

        [Column("Descripcion")]
        [StringLength(100)]
        [Required(ErrorMessage = "El campo {0} es requerido")]
        [DataType(DataType.Text)]
        public string Descripcion { get; set; }
    }
}