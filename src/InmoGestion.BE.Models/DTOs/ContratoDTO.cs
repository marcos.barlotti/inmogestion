﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InmoGestion.BE.Models.DTOs
{
    public class ContratoDTO
    {
        
        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Display(Name="Id")]
        public int Id { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Display(Name="Indexacion")]
        public float? Indexacion { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Display(Name="Indexacion Tiempo")]
        public DateTime IndexacionTiempo { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Display(Name="Valor Alquiler")]
        public float ValorAlquiler { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Display(Name="Fecha Inicio")]
        public DateTime FechaInicio { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Display(Name="Fecha Final")]
        public DateTime FechaFinal { get; set; }
    }
}
