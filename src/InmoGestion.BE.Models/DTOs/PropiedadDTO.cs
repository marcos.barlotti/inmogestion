﻿using InmoGestion.BE.Models.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InmoGestion.BE.Models.DTOs;

namespace InmoGestion.BE.Models.DTOs
{
    public class PropiedadDTO
    {
        
        [Display(Name = "Id")]
        public int Id { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Display(Name = "Id Contrato")]
        public int IdContrato { get; set; }
        public ContratoDTO Contrato { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Display(Name = "Id Propietario")]
        public int IdPropietario { get; set; }
        public PropietarioDTO Propietario { get; set; }


        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Display(Name = "Id DireccionDTO")]
        public int IdDireccion { get; set; }
        public DireccionDTO Direccion { get; set; }


        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Display(Name = "Valor Venta")]
        public float? ValorVenta { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Display(Name = "Id Tipo Propiedad")]
        public int IdTipoPropiedad { get; set; }
        public TipoPropiedadDTO TipoPropiedad { get; set; }
    }
}
