﻿using InmoGestion.BE.Models.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InmoGestion.BE.Models.DTOs
{
    public class PersonaDTO
    {
        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Display(Name = "Id")]
        public int Id { get; set; }

        [Display(Name = "Nombre")]
        [StringLength(100)]
        [Required(ErrorMessage = "El campo {0} es requerido")]
        [DataType(DataType.Text)]
        public string Nombre { get; set; }

        [Display(Name = "Apellido")]
        [StringLength(100)]
        [Required(ErrorMessage = "El campo {0} es requerido")]
        [DataType(DataType.Text)]
        public string Apellido { get; set; }

        [Display(Name = "Telefono")]
        [StringLength(100)]
        [Required(ErrorMessage = "El campo {0} es requerido")]
        [DataType(DataType.Text)]
        public string Telefono { get; set; }

        [Display(Name = "Mail")]
        [StringLength(100)]
        [Required(ErrorMessage = "El campo {0} es requerido")]
        [DataType(DataType.Text)]
        public string Mail { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Display(Name = "Id_Direccion")]
        public int IdDireccion { get; set; }
        public DireccionDTO Direccion { get; set; }
    }
}
