using AutoMapper;
using InmoGestion.BE.Models.DTOs;
using InmoGestion.BE.Models.Entities;

namespace InmoGestion.BE.Models.Profiles
{
    public class InquilinoProfile : Profile
    {
        public InquilinoProfile()
    {
            CreateMap<Inquilino, InquilinoDTO>()
                .ForMember(destino => destino.Id, option => option.MapFrom(origen => origen.Id))
                .ForMember(destino => destino.IdContrato, option => option.MapFrom(origen => origen.IdContrato))
                .ForMember(destino => destino.Contrato, option => option.MapFrom(origen => origen.Contrato))
                .ForMember(destino => destino.IdPersona, option => option.MapFrom(origen => origen.IdPersona))
                .ForMember(destino => destino.Persona, option => option.MapFrom(origen => origen.Persona))
                .ReverseMap();
    }
    }
}
