using AutoMapper;
using InmoGestion.BE.Models.DTOs;
using InmoGestion.BE.Models.Entities;

namespace InmoGestion.BE.Models.Profiles
{
    public class ContratoProfile : Profile
    {
        public ContratoProfile()
    {
            CreateMap<Contrato, ContratoDTO>()
                .ForMember(destino => destino.Id, option => option.MapFrom(origen => origen.Id))
                .ForMember(destino => destino.Indexacion, option => option.MapFrom(origen => origen.Indexacion))
                .ForMember(destino => destino.IndexacionTiempo, option => option.MapFrom(origen => origen.IndexacionTiempo))
                .ForMember(destino => destino.ValorAlquiler, option => option.MapFrom(origen => origen.ValorAlquiler))
                .ForMember(destino => destino.ValorAlquiler, option => option.MapFrom(origen => origen.ValorAlquiler))
                .ForMember(destino => destino.FechaInicio, option => option.MapFrom(origen => origen.FechaInicio))
                .ForMember(destino => destino.FechaFinal, option => option.MapFrom(origen => origen.FechaFinal))
                .ReverseMap();
    }
    }
}
