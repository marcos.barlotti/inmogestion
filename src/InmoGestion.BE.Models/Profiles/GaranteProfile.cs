using AutoMapper;
using InmoGestion.BE.Models.DTOs;
using InmoGestion.BE.Models.Entities;

namespace InmoGestion.BE.Models.Profiles
{
    public class GaranteProfile : Profile
    {
        public GaranteProfile()
    {
            CreateMap<Garante, GaranteDTO>()
                .ForMember(destino => destino.Id, option => option.MapFrom(origen => origen.Id))
                .ForMember(destino => destino.IdInquilino, option => option.MapFrom(origen => origen.IdInquilino))
                .ForMember(destino => destino.Inquilino, option => option.MapFrom(origen => origen.Inquilino))
                .ForMember(destino => destino.IdPersona, option => option.MapFrom(origen => origen.IdPersona))
                .ForMember(destino => destino.Persona, option => option.MapFrom(origen => origen.Persona))
                .ForMember(destino => destino.IdTipoGarantia, option => option.MapFrom(origen => origen.IdTipoGarantia))
                .ForMember(destino => destino.TipoGarantia, option => option.MapFrom(origen => origen.TipoGarantia))
                .ReverseMap();
    }
    }
}
