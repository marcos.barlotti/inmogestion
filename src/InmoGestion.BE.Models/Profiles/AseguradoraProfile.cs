using AutoMapper;
using InmoGestion.BE.Models.Entities;
using InmoGestion.BE.Models.DTOs;

namespace InmoGestion.BE.Models.Profiles
{
    public class AseguradoraProfile : Profile
    {
        public AseguradoraProfile()
    {
            CreateMap<Aseguradora, AseguradoraDTO>()
                .ForMember(destino => destino.Id, option => option.MapFrom(origen => origen.Id))
                .ForMember(destino => destino.IdInquilino, option => option.MapFrom(origen => origen.IdInquilino))
                .ForMember(destino => destino.Inquilino, option => option.MapFrom(origen => origen.Inquilino))
                .ForMember(destino => destino.RazonSocial, option => option.MapFrom(origen => origen.RazonSocial))
                .ReverseMap();
    }
    }
}
