using AutoMapper;
using InmoGestion.BE.Models.DTOs;
using InmoGestion.BE.Models.Entities;

namespace InmoGestion.BE.Models.Profiles
{
    public class TipoGarantiaProfile : Profile
    {
        public TipoGarantiaProfile()
    {
            CreateMap<TipoGarantia, TipoGarantiaDTO>()
                .ForMember(destino => destino.Id, option => option.MapFrom(origen => origen.Id))
                .ForMember(destino => destino.Descripcion, option => option.MapFrom(origen => origen.Descripcion))
                .ForMember(destino => destino.Recibo, option => option.MapFrom(origen => origen.Recibo))
                .ReverseMap();
    }
    }
}
