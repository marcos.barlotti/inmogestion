using AutoMapper;
using InmoGestion.BE.Models.DTOs;
using InmoGestion.BE.Models.Entities;

namespace InmoGestion.BE.Models.Profiles
{
    public class CatastroProfile : Profile
    {
        public CatastroProfile()
    {
            CreateMap<Catastro, CatastroDTO>()
                .ForMember(destino => destino.Id, option => option.MapFrom(origen => origen.Id))
                .ForMember(destino => destino.Seccion, option => option.MapFrom(origen => origen.Seccion))
                .ForMember(destino => destino.Manzana, option => option.MapFrom(origen => origen.Manzana))
                .ForMember(destino => destino.Parcela, option => option.MapFrom(origen => origen.Parcela))
                .ForMember(destino => destino.Nomenclatura, option => option.MapFrom(origen => origen.Nomenclatura))
                .ReverseMap();
    }
    }
}
