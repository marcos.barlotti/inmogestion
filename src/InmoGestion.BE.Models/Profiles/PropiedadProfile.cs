using AutoMapper;
using InmoGestion.BE.Models.DTOs;
using InmoGestion.BE.Models.Entities;

namespace InmoGestion.BE.Models.Profiles
{
    public class PropiedadProfile : Profile
    {
        public PropiedadProfile()
    {
            CreateMap<Propiedad, PropiedadDTO>()
                .ForMember(destino => destino.Id, option => option.MapFrom(origen => origen.Id))
                .ForMember(destino => destino.IdContrato, option => option.MapFrom(origen => origen.IdContrato))
                .ForMember(destino => destino.Contrato, option => option.MapFrom(origen => origen.Contrato))
                .ForMember(destino => destino.IdPropietario, option => option.MapFrom(origen => origen.IdPropietario))
                .ForMember(destino => destino.Propietario, option => option.MapFrom(origen => origen.Propietario))
                .ForMember(destino => destino.IdDireccion, option => option.MapFrom(origen => origen.IdDireccion))
                .ForMember(destino => destino.Direccion, option => option.MapFrom(origen => origen.Direccion))
                .ForMember(destino => destino.ValorVenta, option => option.MapFrom(origen => origen.ValorVenta))
                .ForMember(destino => destino.IdTipoPropiedad, option => option.MapFrom(origen => origen.IdTipoPropiedad))
                .ForMember(destino => destino.TipoPropiedad, option => option.MapFrom(origen => origen.TipoPropiedad))
                .ReverseMap();
    }
    }
}
