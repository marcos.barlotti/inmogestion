using AutoMapper;
using InmoGestion.BE.Models.DTOs;
using InmoGestion.BE.Models.Entities;

namespace InmoGestion.BE.Models.Profiles
{
    public class PropietarioProfile : Profile
    {
        public PropietarioProfile()
    {
            CreateMap<Propietario, PropietarioDTO>()
                .ForMember(destino => destino.Id, option => option.MapFrom(origen => origen.Id))
                .ForMember(destino => destino.IdPersona, option => option.MapFrom(origen => origen.IdPersona))
                .ForMember(destino => destino.Persona, option => option.MapFrom(origen => origen.Persona))
                .ReverseMap();
    }
    }
}
