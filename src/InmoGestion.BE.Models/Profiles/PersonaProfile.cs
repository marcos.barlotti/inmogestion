using AutoMapper;
using InmoGestion.BE.Models.DTOs;
using InmoGestion.BE.Models.Entities;

namespace InmoGestion.BE.Models.Profiles
{
    public class PersonaProfile : Profile
    {
        public PersonaProfile()
    {
            CreateMap<Persona, PersonaDTO>()
                .ForMember(destino => destino.Id, option => option.MapFrom(origen => origen.Id))
                .ForMember(destino => destino.Nombre, option => option.MapFrom(origen => origen.Nombre))
                .ForMember(destino => destino.Apellido, option => option.MapFrom(origen => origen.Apellido))
                .ForMember(destino => destino.Telefono, option => option.MapFrom(origen => origen.Telefono))
                .ForMember(destino => destino.Mail, option => option.MapFrom(origen => origen.Mail))
                .ForMember(destino => destino.IdDireccion, option => option.MapFrom(origen => origen.IdDireccion))
                .ForMember(destino => destino.Direccion, option => option.MapFrom(origen => origen.Direccion))
                .ReverseMap();
    }
    }
}
