using AutoMapper;
using InmoGestion.BE.Models.DTOs;
using InmoGestion.BE.Models.Entities;

namespace InmoGestion.BE.Models.Profiles
{
    public class DireccionProfile : Profile
    {
        public DireccionProfile()
    {
            CreateMap<Direccion, DireccionDTO>()
                .ForMember(destino => destino.Id, option => option.MapFrom(origen => origen.Id))
                .ForMember(destino => destino.IdCatastro, option => option.MapFrom(origen => origen.IdCatastro))
                .ForMember(destino => destino.Catastro, option => option.MapFrom(origen => origen.Catastro))
                .ForMember(destino => destino.Calle, option => option.MapFrom(origen => origen.Calle))
                .ForMember(destino => destino.Numero, option => option.MapFrom(origen => origen.Numero))
                .ForMember(destino => destino.Piso, option => option.MapFrom(origen => origen.Piso))
                .ForMember(destino => destino.Departamento, option => option.MapFrom(origen => origen.Departamento))
                .ReverseMap();
    }
    }
}
