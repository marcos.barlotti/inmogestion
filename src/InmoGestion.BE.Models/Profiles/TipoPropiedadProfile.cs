using AutoMapper;
using InmoGestion.BE.Models.DTOs;
using InmoGestion.BE.Models.Entities;

namespace InmoGestion.BE.Models.Profiles
{
    public class TipoPropiedadProfile : Profile
    {
        public TipoPropiedadProfile()
    {
            CreateMap<TipoPropiedad, TipoPropiedadDTO>()
                .ForMember(destino => destino.Id, option => option.MapFrom(origen => origen.Id))
                .ForMember(destino => destino.Descripcion, option => option.MapFrom(origen => origen.Descripcion))
                .ReverseMap();
    }
    }
}
