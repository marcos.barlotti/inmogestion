﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InmoGestion.BE.Models.Entities
{
    [Table(name: "Aseguradora")]
    public class Aseguradora
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Column("Id")]
        public int Id { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Column("Id_Inquilino")]
        public int IdInquilino { get; set; }
        public Inquilino Inquilino { get; set; }


        [Column("Razon_Social")]
        [StringLength(100)]
        [Required(ErrorMessage = "El campo {0} es requerido")]
        [DataType(DataType.Text)]
        public string RazonSocial { get; set; }
    }
}
