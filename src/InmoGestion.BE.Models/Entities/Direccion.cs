﻿using InmoGestion.BE.Models.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InmoGestion.BE.Models.Entities
{
    [Table(name: "Direccion")]
    public class Direccion
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Column("Id")]
        public int Id { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Column("Id_Catastro")]
        public int IdCatastro { get; set; }
        public Catastro Catastro { get; set; }

        [Column("Calle")]
        [StringLength(100)]
        [Required(ErrorMessage = "El campo {0} es requerido")]
        [DataType(DataType.Text)]
        public string Calle { get; set; }

        [Column("Numero")]
        [Required(ErrorMessage = "El campo {0} es requerido")]
        public int Numero { get; set; }

        [Column("Piso")]
        [Required(ErrorMessage = "El campo {0} es requerido")]
        public int Piso { get; set; }

        [Column("Departamento")]
        [StringLength(100)]
        [Required(ErrorMessage = "El campo {0} es requerido")]
        [DataType(DataType.Text)]
        public string Departamento { get; set; }
    }
}