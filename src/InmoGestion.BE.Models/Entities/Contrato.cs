﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InmoGestion.BE.Models.Entities
{
    [Table(name: "Contrato")]
    public class Contrato
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Column("Id")]
        public int Id { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Column("Indexacion")]
        public float? Indexacion { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Column("Indexacion_Tiempo")]
        public DateTime IndexacionTiempo { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Column("Valor_Alquiler")]
        public float ValorAlquiler { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Column("Fecha_Inicio")]
        public DateTime FechaInicio { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Column("Fecha_Final")]
        public DateTime FechaFinal { get; set; }

    }
}
