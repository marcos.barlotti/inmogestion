﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InmoGestion.BE.Models.Entities;

namespace InmoGestion.BE.Models.Entities
{
    [Table(name: "Propiedad")]
    public class Propiedad
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Column("Id")]
        public int Id { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Column("Id_Contrato")]
        public int IdContrato { get; set; }
        public Contrato Contrato{ get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Column("Id_Propietario")]
        public int IdPropietario { get; set; }
        public Propietario Propietario { get; set; }


        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Column("Id_Direccion")]
        public int IdDireccion { get; set; }
        public Direccion Direccion { get; set; }


        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Column("Valor_Venta")]
        public float? ValorVenta{ get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Column("Id_Tipo_Propiedad")]
        public int IdTipoPropiedad { get; set; }
        public TipoPropiedad TipoPropiedad { get; set; }
    }
}