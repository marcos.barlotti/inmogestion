﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace InmoGestion.BE.Models.Entities
{
    [Table(name: "Inquilino")]
    public class Inquilino
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Column("Id")]
        public int Id { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Column("Id_Contrato")]
        public int IdContrato { get; set; }
        public Contrato Contrato { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Column("Id_Persona")]
        public int IdPersona { get; set; }
        public Persona Persona { get; set; }
    }
}