﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InmoGestion.BE.Models.Entities
{
    [Table(name: "Garante")]
    public class Garante
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Column("Id")]
        public int Id { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Column("Id_Inquilino")]
        public int IdInquilino { get; set; }
        public Inquilino Inquilino { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Column("Id_Persona")]
        public int IdPersona { get; set; }
        public Persona Persona { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Column("Id_Tipo_Garantia")]
        public int IdTipoGarantia { get; set; }
        public TipoGarantia TipoGarantia { get; set; }
    }
}
